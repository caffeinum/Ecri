var a,b,c,d;

$(function () {
	var editor = new Ecri( $('.editor').text() );
	
	editor.text( "Hi, this is demo of Ecri editor" );
	
	a = new Ecri( 'text' );
	b = new Ecri( 'btext' );
	a.append( b );
	b.varyOne( c = new Ecri('ctext') );
	d = new Ecri( 'dtext' );
	d.prepend( b, c );
	
	var view = new View('main', a);
});

/* docs */
/*

	|
	a
	|
	|\
	| \
	b c
	| /
	|/
	|
	d
	|
	
	a = new Ecri( text );
	a.append( b );
	b.vary( c );
	d = new Ecri( text );
	d.appendTo( b, c );
	
	/
	
	
	
	*/