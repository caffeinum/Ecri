var View = function (selector, root) {
	this.jq = $(selector);
	if ( ! this.jq ) console.error('Empty selector');
	
	this.root = root || new Ecri( this.jq.text() );
	this.init( this.root );
	this.initChildren( this.root );
};

View.prototype = {
	elements: [],
	init: function (elem) {
		var id = this.elements.push( elem ) - 1;
		elem.id( id );
		$("<div>")
		.attr(		"id",	"ecri-"+id)
		.addClass("ecri")
		.text(		elem.text())
		.appendTo(	this.jq )
		.click(this.edit.bind(this,elem,id));
	},
	initChildren: function (elem) {
		if (elem.next()		&&	
		 	elem.next()		.length ) {
			elem.next()		.forEach( this.init.bind(this) );
			elem.next()		.forEach( this.initChildren.bind(this) );
		}
		if (elem.previous()	&&
			elem.previous()	.length ) {
			elem.previous()	.forEach( this.init.bind(this) );
			elem.previous()	.forEach( this.initChildren.bind(this) );
		}
	},
	draw: function (elem, id) {
		id = elem.id();
		$("#ecri-"+id).text(elem.text());
	},
	drawChildren: function (elem) {
		if (elem.next() 		&&	
		 	elem.next()		.length ) {
			elem.next()		.forEach( this.draw.bind(this) );
			elem.next()		.forEach( this.drawChildren.bind(this) );
		}
		if (elem.previous()	&&
			elem.previous()	.length ) {
			elem.previous()	.forEach( this.draw.bind(this) );
			elem.previous()	.forEach( this.drawChildren.bind(this) );
		}
	},
	edit: function (elem, id) {
		id = elem.id();
		var $ecri = $("#ecri-"+id);
		$ecri.hide();
		$('<textarea>')
		.attr("id",	"ecri-edit-"+id)
		.addClass('ecri')
		.html( elem.text() )
		.insertAfter( $ecri )
		.focus()
		.blur(this.save.bind(this,elem,id));		
		
	},
	save: function (elem, id) {
		id = elem.id();
		var $ecri = $("#ecri-"+id);
		var $text = $("#ecri-edit-"+id);
		elem.edit( $text.val() );
		this.draw(elem, id);
		$text.remove();
		$ecri.show();
	},
	render: function () {
		this.draw( this.root );
		this.drawChildren( this.root );
	},
};
